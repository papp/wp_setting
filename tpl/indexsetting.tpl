{function name=fSETTING}
	{foreach name=d key=kd from=$D item=d}
		{if !isset($d.D)}
			<li class="input-group" style="width:100%">
				{if $d.TYPE == 'checkbox'}
				<span class="input-group-addon" style="width:20px"><input type="checkbox" value="1" {if $d.VALUE}checked{/if}></span>
				<span class="form-control">{$kd}</span>
				{else}
				<span class="input-group-addon" style="width:20%">{$kd}</span>
				<input type="text" value="{$d.VALUE}" class="form-control" placeholder="{$kd}" >
				{/if}
			</li>
		{else}
			<br>
			<div class="list-group">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">{$kd}</h4>
					<ul class="list-group-item-text">
						{fSETTING D=$d.D}
					</ul>
				</div>
			</div>
		{/if}
	{/foreach}
{/function}

<form>
	<div class="panel panel-default">
		<div class="panel-heading">SETTING</div>
		<div class="panel-body">
			{fSETTING D=$D.SETTING.D}
		</div>
	</div>
	
	<div class="btn-group" role="group" aria-label="...">
		<button type="button" class="btn btn-default">OK</button>
		<button type="button" class="btn btn-default">Abbrechen</button>
	</div>
</form>